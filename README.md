# Más Aplicaciones

Algunas de mis aplicaciones favoritas:

### Seguime
Encuentra tu dispositivo usando tu propio servidor de rastreo. (Disponible en F-Droid)
* https://gitlab.com/javipc/seguime

### Wifi automágico
Conecta con redes abiertas de forma automática.
* https://gitlab.com/javipc/wifi-automagico

### rutera
Velocímetro para deportistas, almacena las coordendas y muestra un "mapa de velocidad".
* https://gitlab.com/javipc/rutera.

### Fotoclima
El clima en el fondo de pantalla.
* https://gitlab.com/javipc/fotoclima


### ¡Quiero Música!
RoBot para Telegram que permite escuchar y descargar música.
* https://gitlab.com/javipc/quieromusica
* http://t.me/QuieroMusicaBot


## Más Programas

Para programadores.

### Anti robots
Diferencia usuarios humanos de autómatas en tus sitios. (PHP)
* https://gitlab.com/javipc/antirobots

### JBDD
Manejador de base de datos para PHP. 
* https://gitlab.com/javipc/jbdd


## ¿Más... ?

### Asistente MIXXX
Realiza mezclas automáticas o manuales sincronizando el ritmo.
* https://gitlab.com/javipc/autodj


### Kawaii Tokei
Robot que ofrece la hora en fotografías.
Permite ser agregado a grupos y ofrece la hora cuando ingresan usuarios.
* http://t.me/FotoReloj





# Muy agradecido con los usuarios que colaboran

A pesar de que mis aplicaciones tienen poca difusión, son desconocidas, cada tanto me sorprende gente que colabora con donaciones.
Estoy muy agradecido con todos.

* Podes colaborar difundiendo tus programas favoritos, recomienda a tus amigos o en tus redes los programas libres que uses. Si no tienen publicidad ni suscripciones no tienen ingresos.
* También se puede colaborar haciendo donaciones a los programadores. La mayoría de los programadores también colaboramos (un pequeño gesto) con los desarrolladores de la aplicaciones que usamos para nuestros proyectos y para nuestro uso cotidiano.





### Moneda: Peso Argentino (ARS)
<pre>Mercado Pago / Transferencia bancaria
CVU: javier.mp
</pre>

#### Plataforma: Binance Pay
<pre>ID: 222634822
Alias: javicel
</pre>

#### Plataforma: Wise
<pre>ID: romanm412
Url: https://wise.com/share/romanm412
</pre>

#### Plataforma: AirTM
<pre>ID: javicel
Url: https://app.airtm.com/ivt/javicel
</pre>

#### Moneda: BNB | USDT | USDC | DAI
<pre>Red: BEP20 - BNB Smart Chain (Favorita)
Dirección: 0x1c92D34312f4449D5368Ad8b81E91F902cEE3Bbc
</pre>

#### PayPal
<pre>PayPal cobra comisión para transferir.
Usá cripto (:
</pre>

¡Gracias por llegar hasta aquí!  👋🏿

